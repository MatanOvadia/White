function backPressed() {

    if (exitDialog && exitDialog.isShown) {
        exitDialog.hide();
        return;
    }

    if (VideoPlayer.isActive)
        VideoPlayer.stop();

    replaceFocus(back);
}

function enterPressed() {
    var focusedElement = $('.focused');

    if (focusedElement[0] && focusedElement[0].hasAttribute('enter')) {

        if (focusedElement.parents()[0].id === "homePage_header")
            OnFocusedService.handlingHeaderIconSwitchPage(focusedElement);
        else
            replaceFocus(enter);
    }
}

function leftPressed() {
    replaceFocus(left);
}

function rightPressed() {
    replaceFocus(right);
}

function upPressed() {
    if (exitDialog && exitDialog.isShown) {
        return;
    }
    replaceFocus(up);
}

function downPressed() {
    if (exitDialog && exitDialog.isShown) {
        return;
    }
    replaceFocus(down);
}

function playPressed() {
    if (scope.currentTab !== liveTab)
        switch (scope.currentPage) {
            case tvPage:
                if (tvPageManager.lastPlayerType === playerType.ytPlayer)
                    WooYoutubePlayer.playContinue();
                else
                    WooJwPlayer.playContinue();
                break;

            case radioPage:
                WebAudioPlayer.playContinue();
                break;
        }
    else
        AvVideoPlayer.playContinue();
}

function pausePressed() {
    VideoPlayer.pause();
}