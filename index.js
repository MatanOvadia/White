// +------------------------------------------------------------------------+
// // | WooMedia ( www.woo.media )
// +------------------------------------------------------------------------+
// | Woo.Media IS NOT FREE SOFTWARE
// | If you have downloaded this software from a website other
// | than Woo.Media or if you have received
// | this software from someone who is not a representative of
// | Woo.Media, you are involved in an illegal activity.
// | ---
// | In such case, please contact: Hello@Woo.Media
// +------------------------------------------------------------------------+
// | Developed by: Woo.Media / Hello@Woo.Media
// | Copyright: (c) 2017 Woo.Media. All rights reserved.
// +------------------------------------------------------------------------+

'use strict';

var app = angular.module('app', ['ngSanitize']);

app.controller('appController', function ($scope, $log, $timeout, $compile, $http, $q) {

    allowPress = false;

    que = $q;
    http = $http;
    scope = $scope;
    compile = $compile;
    timeoutService = $timeout;

    //      Paging
    scope.currentPage = tvPage;

    scope.showGifSplashScreen = true;

    initScopeVars();

    initSpecialFunctions();

    initScopeFunctions();

    iniAppConfiguration();

    initMouseService();

    app.keyPressController();

   // TODO release after mondial deprecated initFourKShows();

    // TODO matan - release when want to take the live urls from backend
    // HttpOutService.initDynamicLiveUrls();

    scope.formatNowShow = FormatterService.fixCalendarTimeToHours;

    allowPress = true;
});