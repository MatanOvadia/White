const infoBar2Id = 'infoBar2';

app.directive('infoBar2', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'directives/common/infoBar2/infoBar2.html',
        compile: function () {
            return {
                post: function () {
                    if (scope.focusInfoBar2) {
                        FocusService.unfocusCurrent();
                        FocusService.focusByElementId(infoBar2Id);
                        scope.focusInfoBar2 = false;
                    }
                    InfoBar2.initInfoBarState(true);
                }
            }
        }
    }
});

const InfoBar2 = {

    stateIsPause: false,

    focusPlay: function () {
        if (InfoBar2.stateIsPause &&
            $('#infoBar2').length > 0) {
            OnFocusedService.focusMy('#infoBar2', 'play');
            InfoBar2.stateIsPause = false;
        }
    },

    focusPause: function () {
        if (!InfoBar2.stateIsPause &&
            $('#infoBar2').length > 0) {
            OnFocusedService.focusMy('#infoBar2', 'pause');
            InfoBar2.stateIsPause = true;
        }
    },

    progress: function (currentTimeInSec, totalTimeInSec) {
        var percentage = currentTimeInSec / totalTimeInSec;
        var progressBarWidth = percentage * $('#progressBar').width();
        $('#progressBarProgress').css('width', progressBarWidth.toString());
        $('#currentTimeShow').html(FormatterService.fixSecondsTimeFormat(currentTimeInSec));
        $('#endTimeShow').html(FormatterService.fixSecondsTimeFormat(totalTimeInSec - currentTimeInSec));
    },

    clear: function (isLive) {
        if (isLive)
            InfoBar2.progress(0, Infinity);
        else
            InfoBar2.progress(0, 0.1);
    },

    initInfoBarState: function (isHidden) {
        log('initInfoBarState');
        if(!isHidden){
            log('show info bar true');
            scope.showInfoBar = true;
            safeDigest();
        }
        InfoBar2.stateIsPause = true;
        if ($('#infoBar2').length > 0) {
            OnFocusedService.focusMy('#infoBar2', 'init', 'infoBar2Pause')
        }
    }
};