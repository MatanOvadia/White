app.directive('singleOptionDialog', function() {
    return {
        restrict:'E',
        scope: true,
        templateUrl: 'directives/common/dialogs/singleOptionDialog/singleOptionDialog.html'
    }
});