app.directive('dualOptionDialog', function() {
    return {
        restrict:'E',
        scope: true,
        templateUrl: 'directives/common/dialogs/dualOptionDialog/dualOptionDialog.html'
    }
});