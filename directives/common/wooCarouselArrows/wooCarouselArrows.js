app.directive('wooCarouselArrows', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'directives/common/wooCarouselArrows/wooCarouselArrows.html',
        compile: function () {
            return {
                post: function () {
                }
            }
        }
    }
});