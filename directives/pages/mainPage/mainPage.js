var reshetAllSeries;
var reshetRecentShows;
var carouselsContent;

app.directive('mainPage', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'directives/pages/mainPage/mainPage.html',
        compile: function () {

            return {
                pre: function () {

                    AllowPressService.dontAllow(switchersName.reshetTab);

                    scope.showInfoBar = false;
                    scope.showProgressBar = true;
                    scope.infoBarEnter = specialAttribute.playContinue;
                    scope.infoBarUp = "none";
                    scope.infoBarBack = 'elementBeforePlay, ' + specialOrders.stop;
                    scope.infoBarRight = "fastForward, showInfoBar";
                    scope.infoBarLeft = "fastRewind, showInfoBar";

                    var index = 0;
                    for (var i = 0; i < carouselsContent.length; i++) {

                        var catTitle = Object.keys(carouselsContent[i])[0];
                        var itemsToSet = carouselsContent[i][catTitle];
                        if (itemsToSet.length > 5) {
                            index++;
                            var carouselMovementObj = new CarouselMovement(
                                "wooCarouselItems-" + index,
                                353,
                                6,
                                "wooCarouselDiv-" + index,
                                30,
                                catTitle);
                            scope.carousels.push(carouselMovementObj);
                        }
                    }
                    safeDigest();
                },
                post: function () {

                    IntervalService.hideInfoBar();

                    timeoutService(function () {

                        var index = 0;
                        for (var i = 0; i < carouselsContent.length; i++) {

                            var catTitle = Object.keys(carouselsContent[i])[0];
                            var itemsToSet = carouselsContent[i][catTitle];
                            if (itemsToSet.length > 5) {
                                index++;
                                fixYtThumb(itemsToSet);
                                scope.carousels[index - 1].init($('#reshetRecentShowsKDiv-' + index));
                                scope.carousels[index - 1].putItems(carouselsContent[i][catTitle], index - 1);
                            }
                        }

                        FocusService.focusByElementId("wooCarouselDiv-1");
                        FocusService.checkCarouselFocus(null, $('#wooCarouselDiv-1'), down);
                        AllowPressService.allow(switchersName.reshetTab);
                    });
                }
            };
        }
    }
});