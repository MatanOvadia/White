var tempAttributeI = null;

var ParserService = {

    parseVodShows: function (xml) {

        ParserService.xml = xml;

        var toReturn = [];
        var attributes = $(xml).find('attributes');

        for (var i = 0; i < attributes.length; i++) {
            var attributeI = $(xml).find('attributes:eq(' + i + ')');
            tempAttributeI = attributeI;
            var playUrl = attributeI.find('alternative_streams app');

            if (!playUrl || playUrl.length === 0)
                playUrl = attributeI.find('alternative_streams default');

            if (playUrl && playUrl.length > 0) {
                playUrl = playUrl[0].innerText;
                toReturn.push({
                    title: cleanCDATA(attributeI.find('title')[0].innerText),
                    yt_thumb: attributeI.find('image_asset url')[0].innerText,
                    playUrl: playUrl,
                    desc: cleanCDATA(attributeI.find('description')[0].innerHTML)
                })
            }
        }
        return toReturn;
    },

    parsePodcastsShows: function (xml) {

        ParserService.xml = xml;

        var toReturn = [];
        var attributes = $(xml).find('entry');

        for (var i = 0; i < attributes.length; i++) {
            var attributeI = $(xml).find('entry:eq(' + i + ')');
            tempAttributeI = attributeI;
            var playUrl = attributeI.find('applicaster\\:mediaGroup:eq(1) applicaster\\:mediaItem:eq(0)')[0].attributes.src.value;

            var dateNeedToParse = attributeI.find('published')[0].innerText;
            var year = dateNeedToParse.substring(0, 4);
            var month = dateNeedToParse.substring(5, 7);
            var day = dateNeedToParse.substring(8, 10);
            var date = day + '.' + month + '.' + year;
            var dateTime = dateNeedToParse.substring(11, 16);

            if (playUrl && playUrl.length > 0) {
                toReturn.push({
                    title: attributeI.find('title')[0].innerText,
                    yt_thumb: attributeI.find('applicaster\\:mediaGroup:eq(1) applicaster\\:mediaItem:eq(1)')[0].attributes.src.value,
                    playUrl: playUrl,
                    desc: attributeI.find('summary')[0].innerHTML,
                    date: date,
                    dateTime: dateTime
                })
            }
        }
        return toReturn;
    },

    parseNewUrlXml: function (xmlResponse) {
        var metadata = $($.parseXML(xmlResponse)).find('Metadata');
        var playbackLinks = metadata.find('PlaybackLinks');
        var smilUrl = playbackLinks.find('SmilURL');
        var baseUrl = smilUrl[0].textContent;
        var server1 = metadata.children('cdninfo').children('servers').children()[0].textContent;
        var toReplaceHostName = ParserService.private.extractHostname(baseUrl);
        var finalUrl = baseUrl.replace(toReplaceHostName, server1);

        return [finalUrl];
    },

    private: {
        extractHostname: function (url) {
            var hostname;
            //find & remove protocol (http, ftp, etc.) and get hostname
            if (url.indexOf("://") > -1)
                hostname = url.split('/')[2];
            else
                hostname = url.split('/')[0];
            //find & remove port number
            hostname = hostname.split(':')[0];
            //find & remove "?"
            hostname = hostname.split('?')[0];
            return hostname;
        }
    }
};

function cleanCDATA(string) {
    string = string.replace('<!--[CDATA[', '');
    string = string.replace('<![CDATA[', '');
    string = string.replace(']]-->', '');
    string = string.replace(']]>', '');
    return string;
}