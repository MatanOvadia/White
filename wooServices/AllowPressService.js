var allowPress = false;

const AllowPressService = {

    init: function () {
        allowPress = true;
    },

    allowPressBlockingCount: 0,

    dontAllow: function (switcherName, noSplash) {
        if (!switcherName)
            console.error('switcher name is not defined');

        allowPress = false;
        if (!noSplash)
            PreloaderSplashService.show();
        AllowPressService.switchers[switcherName] = false;
    },

    allow: function (switcherName) {
        AllowPressService.switchers[switcherName] = true;
        allowPress = !AllowPressService.isThereOffSwitchers();
        if (allowPress)
            PreloaderSplashService.hide();
    },

    isThereOffSwitchers: function () {
        var foundOffSwitcher;
        Object.keys(AllowPressService.switchers).forEach(function (switcher) {
            if (!AllowPressService.switchers[switcher]) {
                foundOffSwitcher = true;
                console.warn('switcher: ' + switcher + ', is off');
            }
        });
        return  foundOffSwitcher;
    },

    switchers: {
        homePage: true,
        liveTab: true,
        reshetTab: true,
        vodTab: true,
        fourK: true,
        vodInner: true,
        seriesInner: true,
        closeInner: true,
        vodSeriesInnerClose: true,
        app: true,
        focusElement: true,
        jwFullScreen: true,
        closeReshetInner: true,
        podcastsInner: true,
        podcastsInnerClose: true,
        jwPlay: true,
        carouselPutItems: true,
        carouselPutItemsMoreThan30: true,
        getXml: true,
        initLiveShowCalender: true,
        jsonRequest: true,
        prepare: true,
        onload: true,
        dialog: true,
        epg: true
    }
};

const switchersName = {
    homePage: 'homePage',
    liveTab: 'liveTab',
    reshetTab: 'reshetTab',
    vodTab: 'vodTab',
    vodInner: 'vodInner',
    seriesInner: 'seriesInner',
    podcastsInner: 'podcastsInner',
    app: 'app',
    focusElement: 'focusElement',
    jwFullScreen: 'jwFullScreen',
    fourK: 'fourK',
    vodSeriesInnerClose: 'vodSeriesInnerClose',
    closeReshetInner: 'closeReshetInner',
    closeInner: 'closeInner',
    podcastsInnerClose: 'podcastsInnerClose',
    jwPlay: 'jwPlay',
    carouselPutItems: 'carouselPutItems',
    carouselPutItemsMoreThan30: 'carouselPutItemsMoreThan30',
    getXml: 'getXml',
    initLiveShowCalender: 'initLiveEpg',
    jsonRequest: 'jsonRequest',
    prepare: 'prepare',
    onload: 'onload',
    scrollSwitcher: ' scrollSwitcher',
    click: ' click',
    dialog: 'dialog',
    epg: 'epg'
};

if (typeof onloadFinished !== 'undefined')
    AllowPressService.dontAllow(switchersName.onload);