const CarouselGridService = {

    private: {
        baseMarginTop: 540, // px = 50vh
        carouselHeight: 271.797 //px
    },

    updateGridTop: function () {
        var carouselMultiplier = FocusService.getCurrentFocusedCarouselIndex();

        var titleHeight = 40;
        var carouselTotalSize = CarouselGridService.private.carouselHeight + titleHeight;
        var marginTopToSet = (
            CarouselGridService.private.baseMarginTop
            - (carouselMultiplier * carouselTotalSize))
            + "px";

        $('#mainPage').animate({'marginTop': marginTopToSet}, 500, "easeInOutQuad");
    },

    getCurrentFocusedCarousel: function () {
        return scope.carousels[FocusService.getCurrentFocusedCarouselIndex()];
    }
};