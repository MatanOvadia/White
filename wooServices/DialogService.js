const DialogService = {

    hideNotConnected: function () {
        if (scope.showSingleOptionDialog) {
            scope.showSingleOptionDialog = false;
            safeDigest();
        }
    },

    defineSingleDialogScope: function () {
        scope.singleOptionDialogMsg = 'החיבור לאינטרנט אינו תקין, צא או המתן';
        scope.singleOptionDialogBtnMsg = 'יציאה';
    },

    defineDualDialogScope: function (dialogMsg,
                                     rightOptionText,
                                     rightOptionOnEnterAttribute,
                                     leftOptionText,
                                     leftOptionOnEnterAttribute) {
        scope.dialogMsg = dialogMsg;
        scope.dialogRightBtnText = rightOptionText;
        scope.dialogRightEnter = rightOptionOnEnterAttribute;
        scope.dialogLeftBtnText = leftOptionText;
        scope.dialogLeftEnter = leftOptionOnEnterAttribute;
        safeDigest();
    },

    playingError: function () {
        MouseService.dontAllowClicks();
        DialogService.defineDualDialogScope(
            'Video is not available',
            'Back',
            specialAttribute.elementBeforePlay,
            'Home',
            specialAttribute.elementBeforePlay);
        scope.showInfoBar = false;
        scope.showDualDialog = true;
        scope.safeDigest();
        FocusService.unfocusCurrent();
        FocusService.focusByElementId('exitDialog_no');
    }
};
