const MouseService = {

    isAllowedClicks: false,

    isBinding: false,

    focusMe: function (id) {
        FocusService.unfocusCurrent();
        var focusElement = $('#' + id);
        FocusService.focusElement(focusElement);
        return focusElement;
    },

    bindLiveEvents: function () {
        if (!MouseService.isBinding) {
            MouseService.isBinding = true;

            body.click(function () {
                if (MouseService.isAllowedClicks)
                    VideoPlayer.playPause();
            });
            body.mousemove(function () {
                if (!scope.showInfoBar) {
                    console.log('showInfoBar = true');
                    log('show info bar true');
                    scope.showInfoBar = true;
                    scope.safeDigest();
                }
            });
        }
    },

    bindYtClickEvents: function () {
        if (!MouseService.isBinding) {
            MouseService.isBinding = true;
            body.click(function () {
                WooYoutubePlayer.playPause();
            });
            body.mousemove(function () {
                if (!scope.showInfoBar) {
                    console.log('showInfoBar = true');
                    log('show info bar true');
                    scope.showInfoBar = true;
                    scope.safeDigest();
                }
            });
        }
    },

    unbindClickEvents: function () {
        if (MouseService.isBinding) {
            MouseService.isBinding = false;
            body.unbind('mousemove');
            body.unbind('click');
        }
    },

    allowClicks: function () {
        MouseService.isAllowedClicks = true;
    },

    dontAllowClicks: function () {
        MouseService.isAllowedClicks = false;
        MouseService.unbindClickEvents();
    }
};

function initMouseService() {

    scope.showInfoBarFunc = function () {
        if (!MouseService.isAllowedClicks)
            return;
        console.log('showInfoBar = true');
        log('show info bar true');
        scope.showInfoBar = true;
        scope.safeDigest();
    };

    scope.arrowNextPressed = function () {
        if (!MouseService.isAllowedClicks)
            return;
        switch (scope.currentTab) {
            case vodTab:
                carousel_movement3.pushRight();
                break;

            case fourKTab:
                carousel_movement1.pushRight();
                break;

            case reshetShowsTab:
                carousel_movement2.pushRight();
                break;
        }
    };

    scope.arrowPrevPressed = function () {
        if (!MouseService.isAllowedClicks)
            return;
        switch (scope.currentTab) {
            case vodTab:
                carousel_movement3.pushLeft();
                break;

            case fourKTab:
                carousel_movement1.pushLeft();
                break;

            case reshetShowsTab:
                carousel_movement2.pushLeft();
                break;
        }
    };

    scope.playOnClick = function (event) {
        if (!MouseService.isAllowedClicks)
            return;
        AllowPressService.dontAllow(switchersName.click, true);
        MouseService.dontAllowClicks();
        var currentCarouselItemElement = $(event.currentTarget);
        var myIndex = Number(currentCarouselItemElement.attr('myIndex'));
        console.log(myIndex);
        CarouselService.setMainShowByItem(
            CarouselGridService.getCurrentFocusedCarousel().itemsArr[myIndex]);
        VideoPlayer.playAndShowInfoBar(CarouselGridService.getCurrentFocusedCarousel().itemsArr[myIndex].streamURL);
        AllowPressService.allow(switchersName.click);
    };

    scope.focusMeAndEnter = function (event, isMaster) {
        if (!MouseService.isAllowedClicks && !isMaster)
            return;
        var id = event.currentTarget.id;
        $('.video-item').removeClass(active);
        MouseService.focusMe(id);
        replaceFocus(enter);
    };

    scope.focusHeaderElementFunc = function (id) {
        if (!MouseService.isAllowedClicks)
            return;
        var focusHeaderElement = MouseService.focusMe(id);
        $('.video-item').removeClass(active);
        OnFocusedService.handlingHeaderIconSwitchPage(focusHeaderElement);
    };

    scope.mouseInfoBarClick = function () {
        if (!MouseService.isAllowedClicks)
            return;
    };
}