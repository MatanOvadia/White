const EpgService = {
    
    setNowShow: function (title) {
        scope.nowShow = {
            title: title
        }
    },

    setNowNextShows: function (calender) {
        var tabCallMe = scope.currentTab;
        var nowDate = new Date();
        log(nowDate);

        var currentItem;
        var indexOfNowShow = -1;
        for (var i = 0; i < calender.length; i++) {
            currentItem = calender[i];
            var d1;
            var d2;

            if (tizen2016) {
                d1 = new Date(currentItem.schedule_date);
                d2 = new Date(currentItem.schedule_end_time);
                d1 = d1.getTime() + tizen.time.getCurrentDateTime().secondsFromUTC() * 1000;
                d2 = d2.getTime() + tizen.time.getCurrentDateTime().secondsFromUTC() * 1000;
            } else {
                d1 = new Date(currentItem.schedule_date + "z");
                d2 = new Date(currentItem.schedule_end_time + "z");
                d1.addHours(-1 * wooConfig.gmt);
                d2.addHours(-1 *wooConfig.gmt);
            }

            if (nowDate > d1 && nowDate < d2) {
                console.log('currentItem');
                console.log(currentItem);
                scope.nowShow = currentItem;
                indexOfNowShow = i;
                if (i < calender.length - 1) {
                    scope.nextShow = calender[i + 1];
                }
                setTimeout(function () {
                    if (scope.currentTab === tabCallMe) {
                        EpgService.setNowNextShows(calender)
                    }
                }, d2 - nowDate + 1000);
                safeDigest();
                break;
            }
        }
        if (indexOfNowShow === -1)
            scope.nowShow = {title: "המידע אינו זמין"};
        return indexOfNowShow;
    }

};