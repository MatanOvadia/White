const IntervalService = {

    lastKeyDownTimestamp: 0,
    hideInfoBarInterval: null,

    hideInfoBar: function () {
        IntervalService.hideInfoBarInterval =
            setInterval(function () {
                var t = new Date();
                var tMinus12Sec = t.setSeconds(t.getSeconds() - 5);
                if (scope.currentPage === tvPage &&
                    tMinus12Sec > IntervalService.lastKeyDownTimestamp) {
                    scope.showInfoBar = false;
                    scope.safeDigest();
                }
            }, 3000);
    }
};