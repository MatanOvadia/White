const specialAttribute = {
    exit: 'exit',
    grid: 'grid',
    innerPage: 'innerPage',
    playUrl: 'playUrl',
    playContinue: 'playContinue',
    audioPlayContinue: 'audioPlayContinue',
    pause: 'pause',
    stop: 'stop',
    none: 'none',
    elementBeforePlay: 'elementBeforePlay',
    goToVisibleIconEnterNext: 'goToVisibleIconEnterNext',
    fastForward: 'fastForward',
    fastRewind: 'fastRewind',
    showNextShow: 'showNextShow',
    showPrevShow: 'showPrevShow',
    focusPodcastsGrid: 'focusPodcastsGrid',
    unfocusPodcastsGrid: 'unfocusPodcastsGrid',
    setActiveTab: 'setActiveTab',
    nextPage: 'next-page',
    exitDialog_pressNo: 'exitDialog_pressNo',
    exitDialog_pressYes: 'exitDialog_pressYes',
    reloadApp: 'reloadApp',
    exitApp: 'exitApp',
    tryAgainReloadTab: 'tryAgainReloadTab'
};

function checkAttribute(ordersArr, focusedElement, nextFocusedElementID) {

    switch (ordersArr[0]) {

        case specialAttribute.tryAgainReloadTab:
            scope.showDualDialog = false;
            RetryHandler.reloadTab();
            break;

        case specialAttribute.exitApp:
            ExitService.exitApp();
            break;

        case specialAttribute.reloadApp:
            ExitService.reloadApp();
            break;

        case specialAttribute.exitDialog_pressYes:
            exitDialog.hide();
            exitDialog_yes_func();
            return DONT_CONTINUE;

        case specialAttribute.exitDialog_pressNo:
            exitDialog.hide();
            return DONT_CONTINUE;

        case specialAttribute.nextPage:
            if (focusedElement[0] && focusedElement[0].hasAttribute('next-tab'))
                scope.currentTab = focusedElement[0].getAttribute('next-tab');

            var nextPage = focusedElement[0].getAttribute('next-page');
            scope.currentPage = nextPage;
            scope.safeDigest();
            return DONT_CONTINUE;

        case specialAttribute.fastForward:
            VideoPlayer.fastForward();
            return DONT_CONTINUE;

        case specialAttribute.fastRewind:
            VideoPlayer.fastRewind();
            return DONT_CONTINUE;

        case specialAttribute.showNextShow:
            switch (scope.currentTab) {
                case mondialTab:
                    if (MondialLive.calender.length > MondialLive.indexOfNowShow + MondialLive.distanceFromNowShow + 1) {
                        $('.infoBar').addClass('showCarouselMode');
                        MondialLive.distanceFromNowShow++;
                        scope.nowShow = MondialLive.calender[MondialLive.indexOfNowShow + MondialLive.distanceFromNowShow];
                        safeDigest();
                    } else $('.wooCarouselArrow.HE-next').hide();
                    break;

                case liveTab:
                    if (KanLive.calender.length > KanLive.indexOfNowShow + KanLive.distanceFromNowShow + 1) {
                        $('.infoBar').addClass('showCarouselMode');
                        KanLive.distanceFromNowShow++;
                        scope.nowShow = KanLive.calender[KanLive.indexOfNowShow + KanLive.distanceFromNowShow];
                        safeDigest();
                    } else $('.wooCarouselArrow.HE-next').hide();
                    break;

                case arabTab:
                    if (ArabLive.calender.length > ArabLive.indexOfNowShow + ArabLive.distanceFromNowShow + 1) {
                        $('.infoBar').addClass('showCarouselMode');
                        ArabLive.distanceFromNowShow++;
                        scope.nowShow = ArabLive.calender[ArabLive.indexOfNowShow + ArabLive.distanceFromNowShow];
                        safeDigest();
                    } else $('.wooCarouselArrow.HE-next').hide();
            }
            return DONT_CONTINUE;

        case specialAttribute.showPrevShow:
            switch (scope.currentTab) {

                case liveTab:
                    if (0 < KanLive.distanceFromNowShow) {
                        $('.wooCarouselArrow.HE-next').show();
                        if (KanLive.distanceFromNowShow === 1)
                            $('.infoBar').removeClass('showCarouselMode');
                        KanLive.distanceFromNowShow--;
                        scope.nowShow = KanLive.calender[KanLive.indexOfNowShow + KanLive.distanceFromNowShow];
                        safeDigest();
                    }
                    break;

                case arabTab:
                    if (0 < ArabLive.distanceFromNowShow) {
                        $('.wooCarouselArrow.HE-next').show();
                        if (ArabLive.distanceFromNowShow === 1)
                            $('.infoBar').removeClass('showCarouselMode');
                        ArabLive.distanceFromNowShow--;
                        scope.nowShow = ArabLive.calender[ArabLive.indexOfNowShow + ArabLive.distanceFromNowShow];
                        safeDigest();
                    }
                    break;

                case mondialTab:
                    if (0 < MondialLive.distanceFromNowShow) {
                        $('.wooCarouselArrow.HE-next').show();
                        if (MondialLive.distanceFromNowShow === 1)
                            $('.infoBar').removeClass('showCarouselMode');
                        MondialLive.distanceFromNowShow--;
                        scope.nowShow = MondialLive.calender[MondialLive.indexOfNowShow + MondialLive.distanceFromNowShow];
                        safeDigest();
                    }
                    break;
            }
            return DONT_CONTINUE;

        case specialAttribute.stop:
            playerStop();
            break;

        case specialAttribute.goToVisibleIconEnterNext:
            OnFocusedService.focusMy(focusedElement, 'enter-next');
            return DONT_CONTINUE;

        case specialAttribute.elementBeforePlay:
            scope.showDualDialog = false;
            safeDigest();
            FocusService.unfocusCurrent();
            if (focusedElement[0].getAttribute('element-before-play'))
                FocusService.focusByElementId(focusedElement[0].getAttribute('element-before-play'));
            else
                FocusService.focusByElementId(scope.elementBeforePlay);
            break;

        case specialAttribute.none:
            return DONT_CONTINUE;

        case specialAttribute.playContinue:
            specialPlayContinue();
            return DONT_CONTINUE;

        case specialAttribute.audioPlayContinue:
            WebAudioPlayer.playContinue();
            return DONT_CONTINUE;

        case specialAttribute.pause:
            specialPause();
            return DONT_CONTINUE;

        case playUrl:
        case specialAttribute.playUrl:
            PlayUrlService.play();
            return DONT_CONTINUE;

        case specialAttribute.exit:
            ExitService.exit();
            return DONT_CONTINUE;

        case specialAttribute.grid:
            var gridId;
            var gridItemPrefix;
            if (scope.currentTab === reshetShowsTab && scope.innerPage) {
                gridId = "series-row-hold-col-videos";
                gridItemPrefix = "inner-video-num-";
            } else {
                gridId = focusedElement[0].getAttribute('grid-id');
                gridItemPrefix = focusedElement[0].getAttribute('grid-item-prefix');
            }
            var nextFocusedElementNum = GridService.getIndexOfTopRightItem(gridId, gridItemPrefix);
            if (nextFocusedElementNum === -1)
                return DONT_CONTINUE;
            else
                ordersArr[0] = nextFocusedElementNum;
            return gridItemPrefix + ordersArr[0];

        case specialAttribute.innerPage:
            scope.innerPage = true;
            scope.safeDigest();
            return DONT_CONTINUE;

        case lastFocusedElement:
            if (FocusService.lastFocusedElement[0] &&
                FocusService.lastFocusedElement[0].id)
                return FocusService.lastFocusedElement[0].id;
            else {
                console.error("Matan - can't find id for lastFocusedElement. \n" +
                    "FocusService.lastFocusedElement is: ");
                console.log(FocusService.lastFocusedElement);
            }
            break;

        default:
            return ordersArr[0];
    }
}