const specialOrders = {
    tvExitVideoPlayer: 'tvExitVideoPlayer',
    tvStopJwPlayer: 'tvStopJwPlayer',
    tvStopYtPlayer: 'tvStopYtPlayer',
    playPause: 'playPause',
    notInFullScreen: 'notInFullScreen',
    closeInnerPage: 'closeInnerPage',
    stop: 'stop',
    pause: 'pause',
    showInfoBar: 'showInfoBar',
    playContinue: 'playContinue',
    audioPlayContinue: 'audioPlayContinue',
    innerPageFalse: 'innerPageFalse',
    exitVideoPlayer: 'exitVideoPlayer',
    focusVisibleIcon: 'focusVisibleIcon',
    unfocusVisibleIcon: 'unfocusVisibleIcon',
    poscastsPlayerOrder: 'poscastsPlayerOrder',
    exitPodcastsPlayer: 'exitPodcastsPlayer',
    setMain: 'setMain',
    onlyWhenActive: 'onlyWhenActive',
    setActiveTab: 'setActiveTab',
    allowClicks: 'allowClicks'
};

FocusService.checkSpecialCase = function (focusedElement, orderString) {

    var ordersArr = orderString.split(',');

    for (var j = ordersArr.length - 1; j >= 1; j--) {

        switch (ordersArr[j].trim()) {

            case specialOrders.allowClicks:
                MouseService.allowClicks();
                break;

            case specialOrders.setActiveTab:
                if (!focusedElement.hasClass(active)) {
                    ActiveService.setActiveTab(focusedElement);
                    return DONT_CONTINUE;
                }
                break;

            case specialOrders.onlyWhenActive:
                console.log(focusedElement.hasClass(active));
                if (!focusedElement.hasClass(active))
                    return DONT_CONTINUE;
                //NOTE no break in propose

            case specialOrders.exitPodcastsPlayer:
                scope.podcastsPlayer = false;
                safeDigest();
                break;

            case specialOrders.poscastsPlayerOrder:
                scope.podcastsPlayer = true;
                safeDigest();
                break;

            case specialOrders.setMainShow:
                var gridItem = GridService.seriesArr[focusedElement.attr('series-index')];
                OnFocusedService.setMainShow(
                    gridItem.url_flv,
                    gridItem.video_title,
                    gridItem.description.replace(/<(?:.|\n)*?>/gm, ''),
                    gridItem.yt_thumb
                );
                break;

            case specialOrders.unfocusVisibleIcon:
                OnFocusedService.focusMy(focusedElement, 'next');
                break;

            case specialOrders.focusVisibleIcon:
                OnFocusedService.focusMy(focusedElement, 'next');
                break;

            case specialOrders.exitVideoPlayer:
                VideoPlayer.exit();
                break;

            case specialOrders.innerPageFalse:
                scope.innerPage = false;
                break;

            case specialOrders.stop:
                playerStop();
                break;

            case specialOrders.showInfoBar:
                if(VideoPlayer.isActive){
                    console.log('showInfoBar = true');
                    log('show info bar true');scope.showInfoBar = true;
                    scope.safeDigest();
                }
                break;

            case specialOrders.closeInnerPage:
                if (!scope.innerPage) {
                    break;
                }

                switch (scope.currentTab) {

                    case reshetShowsTab:
                        AllowPressService.dontAllow(switchersName.closeReshetInner);
                        scope.innerPage = false;
                        FocusService.unfocusCurrent();
                        FocusService.focusByElementId(reshetShowsTab);
                        scope.safeDigest();
                        AllowPressService.allow(switchersName.closeReshetInner);
                        break;

                    case vodTab:
                        vodSeriesInnerPageManager.close();
                        break;
                }
                break;

            case specialOrders.playContinue:
                specialPlayContinue();
                break;

            case specialOrders.pause:
                specialPause();
                break;

            case specialOrders.tvExitVideoPlayer:
                VideoPlayer.exit();
                break;

            case specialOrders.tvStopJwPlayer:
                if (!scope.currentTab === mondialTab)
                    tvPageManager.tvStopJwPlayer();
                break;

            case specialOrders.tvStopYtPlayer:
                tvPageManager.tvStopYtPlayer();
                break;

            case specialOrders.playPause:
                VideoPlayer.playPause();
                break;

            case specialOrders.notInFullScreen:
                if (WooJwPlayer.isFullscreen() || WooYoutubePlayer.isFullscreen())
                    return DONT_CONTINUE;
                break;
        }
    }

    return checkAttribute(ordersArr, focusedElement, orderString);
};

function playerStop() {
    VideoPlayer.stop();
    scope.showInfoBar = false;
    scope.safeDigest();
}

function specialPlayContinue() {
    switch (tvPageManager.lastPlayerType) {
        case playerType.ytPlayer:
            WooYoutubePlayer.playContinue();
            break;

        case playerType.jwPlayer:
            WooJwPlayer.playContinue();
            break;
    }
}

function specialPause() {

}