const DONT_CONTINUE = false;

var FocusService = {

    lastFocusedElement: null,

    latestFocusedCarousel: null,

    private: {
        getCarouselObj: function (carouselElement) {
            var carouselIndex = Number(carouselElement.attr('id').replace('wooCarouselDiv-', ''));
            return scope.carousels[carouselIndex - 1];
        }
    },

    unfocusCurrent: function () {
        var currentFocusedElement = $('.' + focused);
        FocusService.unfocusElement(currentFocusedElement);
    },

    focusByElementId: function (elementId) {
        log('FocusService -> focusByElementId(' + elementId + ')');
        if (elementId) {
            if (elementId.indexOf('#') === 0)
                FocusService.focusElement($(elementId));
            else
                FocusService.focusElement($('#' + elementId));
        }
    },

    focusElement: function (element) {
        AllowPressService.dontAllow(switchersName.focusElement);
        OnFocusedService.applySpecialOrders(element, 'on-focused');
        element.addClass(focused);
        FocusService.handlingSwitchIcons(null, element);
        AllowPressService.allow(switchersName.focusElement);
    },

    unfocusElement: function (element) {
        if (element.length > 0) {
            OnFocusedService.applySpecialOrders(element, 'on-unfocused');
            element.removeClass(focused);
            FocusService.handlingSwitchIcons(element, null);
            FocusService.lastFocusedElement = element;
        }
    },

    closeInnerPage: function () {
        scope.innerPage = false;
        scope.safeDigest();
    },

    checkCarouselFocus: function (focusedElement, nextFocusedElement, direction) {
        FocusService.checkCarousel(focusedElement, nextFocusedElement, direction);
    },

    checkCarousel: function (focusedElement, nextFocusedElement, direction) {

        var currentIsWooCarouselDiv = focusedElement && focusedElement.hasClass('carouselDiv');
        var nextIsWooCarouselDiv = nextFocusedElement.hasClass('carouselDiv');

        if (!currentIsWooCarouselDiv && nextIsWooCarouselDiv) {
            this.latestFocusedCarousel = FocusService.private.getCarouselObj(nextFocusedElement);
            this.latestFocusedCarousel.focus();
        }

        if (currentIsWooCarouselDiv && !nextIsWooCarouselDiv) {
            var prevCarousel = FocusService.private.getCarouselObj(focusedElement);
            prevCarousel.unfocus();
        }

        if (currentIsWooCarouselDiv && nextIsWooCarouselDiv) {
            this.latestFocusedCarousel = FocusService.private.getCarouselObj(nextFocusedElement);
            var prevCarousel = FocusService.private.getCarouselObj(focusedElement);

            if (prevCarousel ===  this.latestFocusedCarousel) {
                if (direction === left)
                    prevCarousel.pushLeft();
                else if (direction === right)
                    prevCarousel.pushRight();
            } else {
                var prevCenterBalance= prevCarousel.centerBalance;
                var nextCenterBalance=  this.latestFocusedCarousel.centerBalance;
                prevCarousel.unfocus();
                this.latestFocusedCarousel.focus(prevCenterBalance - nextCenterBalance);
                if(direction === down)
                    focusedElement.parent().animate({'opacity': 0}, 600, "easeOutCubic", function () {
                        $(this).css('pointer-events','none');
                    });
                else if(direction === up)
                    nextFocusedElement.parent().animate({'opacity': 1}, 600, "easeInCubic", function () {
                        $(this).css('pointer-events','auto');
                    });
                CarouselGridService.updateGridTop();
            }
        }
    },

    handlingSwitchIcons: function (focusedElement, nextFocusedElement) {

        if (focusedElement && focusedElement.hasClass('icon_switcher'))
            FocusService.iconSwitcherManager.unfocusIcon(focusedElement);

        if (nextFocusedElement && nextFocusedElement.hasClass('icon_switcher'))
            FocusService.iconSwitcherManager.focusIcon(nextFocusedElement);
    },

    iconSwitcherManager: {

        focusIcon: function (element) {
            element.show();
            element.addClass(focused);
            element.children().removeClass('hide');
            $(element[0].children[0]).addClass('hide');
        },

        unfocusIcon: function (element) {
            element.children().removeClass('hide');
            for (var i = 0; i < element.length; i++) {
                $(element[i].children[1]).addClass('hide');
                if ($(element[i]).hasClass('delayRemove')) {
                    (function (i) {
                        setTimeout(function () {
                            if ($(element[i].children[1]).hasClass('hide'))
                                $(element[i]).fadeOut("slow");
                        }, 4000)
                    })(i);
                }
            }
        }
    },

    getCurrentFocusedElementId: function () {
        return $('.focused').attr('id');
    },

    getCurrentFocusedCarouselIndex: function () {
        var carouselElement = $('.focused');
        var carouselId = carouselElement.attr('id');
        return Number(carouselId.replace('wooCarouselDiv-', '')) - 1;
    }
};

function setNextFocusedElementId(focusedElement, direction) {
    if (scope.innerPage &&
        focusedElement[0].hasAttribute(innerPrefix + direction)) {
        return focusedElement[0].getAttribute(innerPrefix + direction);
    } else
        return focusedElement[0].getAttribute(direction);
}

function replaceFocus(direction) {

    var focusedElement = $('.focused');

    if (focusedElement[0] && focusedElement[0].hasAttribute(direction)) {

        var ordersString = setNextFocusedElementId(focusedElement, direction);

        var nextFocusedElementID = FocusService.checkSpecialCase(focusedElement, ordersString);
        if (nextFocusedElementID === DONT_CONTINUE)
            return;

        var andEnter = false;
        if (nextFocusedElementID && nextFocusedElementID.indexOf('AndEnter') > 0) {
            andEnter = true;
            nextFocusedElementID = nextFocusedElementID.replace('AndEnter', '');
        }
        var nextFocusedElementSelector = '#' + nextFocusedElementID;

        if (nextFocusedElementSelector) {

            var nextFocusedElement = $(nextFocusedElementSelector);

            if (nextFocusedElement && nextFocusedElement.length > 0) {

                FocusService.unfocusElement(focusedElement);
                FocusService.focusElement(nextFocusedElement);

                if (andEnter)
                    replaceFocus(enter);

                FocusService.checkCarouselFocus(focusedElement, nextFocusedElement, direction);
            }
        }
    }
}
