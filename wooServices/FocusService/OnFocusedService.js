const OnFocusedService = {

    handlingHeaderIconSwitchPage: function (focusedElement) {
        $('header .active').removeClass(active);
        focusedElement.addClass(active);

        if (scope.currentPage !== focusedElement[0].getAttribute('enter')) {
            VideoPlayer.exit();
            scope.currentTab = '';
        }

        scope.currentPage = focusedElement[0].getAttribute('enter');
        scope.safeDigest();
    },

    applySpecialOrders: function (element, attributeName) {
        var elementOnFocusedAttr = element.attr(attributeName);
        if (elementOnFocusedAttr) {
            FocusService.checkSpecialCase(element, elementOnFocusedAttr);
        }
    },

    focusMy: function (element, myWhat, defaultElementId) {

        var currentImg = $(element).find('img').filter(function () {
            return !($(this).hasClass('hide'));
        });

        var myNext = currentImg.attr('my-' + myWhat);
        if (myNext && myNext !== 'none') {
            $('#' + myNext).removeClass('hide');
            currentImg.addClass('hide');
        }

        if (defaultElementId) { // BUG | 10 | Infobar2  disapear on RadioLive
            currentImg = $(element).find('img').filter(function () {
                return !($(this).hasClass('hide'));
            });
            if (currentImg.length === 0) {
                $('#' + defaultElementId).removeClass('hide');
            }
        }
    },

    setMainShow: function (srcToPlay, title, desc, yt_thumb, mainVideoDuration, mainVideoQuality) {
        EpgService.setNowShow(title);
        scope.srcToPlay = srcToPlay;
        scope.mainImgPath = yt_thumb;
        scope.mainTitle = title;
        scope.mainVideoDuration = mainVideoDuration;
        scope.mainVideoQuality = mainVideoQuality;
        if (desc) {
            desc = desc.replace(/\\n/g, '');
            desc = desc.replace(/\\r/g, '');
        } else {
            desc = '';
        }
        scope.mainDesc = desc;
        scope.safeDigest();
    },

    cleanMainShow: function () {
        scope.srcToPlay = "";
        scope.mainImgPath = "";
        scope.mainTitle = "אנא המתן לטעינת העמוד";
        scope.mainDesc = "";
        scope.safeDigest();
    }
};