var HttpWrapperService = {

    getXml: function (url, successCallback, errorCallback) {
        HttpWrapperService.private.getProxyHttp(
            'xml',
            url,
            successCallback,
            errorCallback);
    },

    getHttp: function (fullUrl, successCallback, errorCallback) {

        if (!successCallback)
            console.error('must define succeess callback');

        if (!errorCallback)
            errorCallback = function () {
                console.error('default error callback. fullUrl = ' + fullUrl);
            };

        (function (isSucceed) {
            var wrapSuccessCallback = function (response) {
                isSucceed = true;
                successCallback(response)
            };
            http.get(fullUrl).then(wrapSuccessCallback, errorCallback);
            timeoutService(function () {
                if (!isSucceed) {
                    errorCallback();
                }
            }, 3000)
        }(false));
    },

    private: {
        getProxyHttp: function (format, url, successCallback, errorCallback) {
            HttpWrapperService.getHttp(
                paths.externalApiCorsProxyUrl +
                "?url=" + url +
                "&format=" + format,
                successCallback,
                errorCallback);
        }
    }
};