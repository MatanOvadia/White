app.directive('preloaderSplash', function () {
    return {
        templateUrl: 'wooServices/PreloaderSplashService/preloaderSplash.html',
        restrict: 'E'
    }
});

const PreloaderSplashService = {

    show: function () {
        scope.preloaderSplash = true;
        scope.safeDigest();
    },

    hide: function () {
        scope.preloaderSplash = false;
        scope.safeDigest();
    }
};