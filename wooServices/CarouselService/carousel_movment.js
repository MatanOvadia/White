/*

how to use :
 JS:
        INIT:
            var numOfItems = stations.length;

             carousel_movement1 = new CarouselMovement("wooCarouselItems", 113, 9, "wooCarouselDiv", 50);
                        carousel_movement1.init($('#fourKDiv'));
                        carousel_movement1.putItems(fourKVideos, 30);
                        carousel_movement1.addToCurrentClass('chosen');

        PRESS:
              function leftPressed() {
                 carousel_movement1.pushLeft();
              }

              function rightPressed() {
                  carousel_movement1.pushRight();
              }
   HTML:
    <div id="wooCarouselDiv">
        <div id="wooCarouselItems"></div>
    </div>

 */

carouselMovementSetup = {

    getVideoTemplate: function (tempData, element) {
        var toReturn = tempData + '<div class="video-grid-con"> <div class="video-item-body">' +
            '<div class="video-item-img" style="position: relative">';

        toReturn += '<img src="' + element.thumbnail + '">';

        // toReturn += '<img class="hideMeIfNotActive" src="resources/img/playIcon.png">';

        toReturn += '</div></div> </div></div></div>';

        return toReturn;
    }
};

function CarouselMovement(carousel, itemWidth, margin, carouselDivId, marginTop, catTitle) {

    var keepItemsArr;
    var myCarousel = null;
    var myWooCarouselDiv = null;
    var numOfItems = 0;
    var currentVideoIndex = 0;
    var skippedImgToIndex;
    var skippedImgFromIndex;

    return {
        id: carouselDivId,
        catTitle: catTitle,
        itemsArr: null,
        centerBalance: -3,
        heightMultiplier: 1.2,

        putItems: function (itemsArr, carouselIndex) {

            if (!itemsArr) {
                console.warn('Call CarouselMovement putItems with undefined array');
                return;
            }
            this.itemsArr = itemsArr;

            $('#wooCarouselArrow-next').hide();
            $('.wooCarouselArrow-next').hide();

            keepItemsArr = itemsArr;
            myCarousel.empty();
            numOfItems = itemsArr.length;

            CarouselService.imgLoadCounter = 0;
            CarouselService.arrSize = numOfItems;

            skippedImgFromIndex = numOfItems - 8;
            skippedImgToIndex = numOfItems;

            for (var i = 0; i < numOfItems; i++) {
                var tempData = CarouselService.getVideoItemDiv(i, carouselIndex);
                tempData = carouselMovementSetup.getVideoTemplate(tempData, itemsArr[i], true);
                myCarousel.append(tempData);
            }
            myCarousel.html(compile(myCarousel.html())(scope));
            currentVideoIndex = numOfItems - 4;
            this.setCss();
        },

        empty: function () {
            myCarousel.empty();
        },

        getLeft: function () {
            var multipleLeft = Math.max(currentVideoIndex -4 - this.centerBalance, 0);
            var developerCustomLeft = 87;
            return ((-1 * (itemWidth  + 2 * margin) * multipleLeft) + margin + developerCustomLeft ) * px_to_vw + 'vw';
        },

        getWidth: function () {
            return ((itemWidth + 2 * margin) * numOfItems) * px_to_vw + 'vw';
        },

        setCss: function () {
            myCarousel.css({
                    'position': 'relative',
                    'height': '100%',
                    'transition': 'left ease-in-out 0.45s',
                    'left': this.getLeft(),
                    'width': this.getWidth()
                }
            );

            $('head').append('<style>' +
                ' .carousel-item{' +
                '                display: inline-block;' +
                '                float: left;\n' +
                '                width: ' + (itemWidth * px_to_vw) + 'vw;\n' +
                '                height: ' + ((itemWidth / 2) * this.heightMultiplier * px_to_vw) + 'vw;\n' +
                '                transition: all 0.45s ease-in-out;\n' +
                '                margin: ' + marginTop * px_to_vw + 'vw ' + margin * px_to_vw + 'vw;\n' +
                '            }\n' +
                '\n' +
                '        .carousel-item.active img:first-child {\n' +
                '                outline: 6px solid ' + active_color + ';\n' +
                '            }\n' +
                '</style>');
        },

        buildCarousel: function (programParent) {
            myCarousel = $('#' + carousel);
            myWooCarouselDiv = $('#' + carouselDivId);
        },

        init: function (programParent) {
            this.buildCarousel(programParent);
        },

        focus: function (centerBalanceDiff) {
            if(centerBalanceDiff){
                currentVideoIndex = currentVideoIndex + centerBalanceDiff;
                this.centerBalance += centerBalanceDiff;
            }
            movement_activation(carousel, currentVideoIndex);
        },

        unfocus: function () {
            $('#' + carousel + ' > .carousel-item:nth-child(' + currentVideoIndex + ')').removeClass('active');
        },

        removeFromAllClass: function (className) {
            $('#' + carousel + ' > .carousel-item').removeClass(className);
        },

        addToCurrentClass: function (className) {
            $('#' + carousel + ' > .carousel-item:nth-child(' + currentVideoIndex + ')').addClass(className);
        },

        getCurrentItemIndex: function () {
            return currentVideoIndex;
        },

        setCurrentItemIndex: function (manualValue) {
            currentVideoIndex = Number(manualValue);
        },

        pushLeft: function () {
            console.log('pushLeft currentVideoIndex = ' + currentVideoIndex);
            if (currentVideoIndex > 1) {
                $('#wooCarouselArrow-next').show();
                $('.wooCarouselArrow-next').show();
                $('#' + carousel + ' > .carousel-item:nth-child(' + (currentVideoIndex) + ')').removeClass('active');
                currentVideoIndex--;
                movement_activation(carousel, currentVideoIndex);
                if (this.centerBalance === -3) {
                    myCarousel.css(left, this.getLeft());
                } else
                    this.centerBalance--;
            } else {
                $('#wooCarouselArrow-prev').hide();
                $('.wooCarouselArrow-prev').hide();
            }
        },

        pushRight: function () {
            console.log('pushRight currentVideoIndex = ' + currentVideoIndex);
            if (currentVideoIndex < numOfItems) {
                $('#wooCarouselArrow-prev').show();
                $('.wooCarouselArrow-prev').show();
                $('#' + carousel + ' > .carousel-item:nth-child(' + (currentVideoIndex) + ')').removeClass('active');
                currentVideoIndex++;
                movement_activation(carousel, currentVideoIndex);
                if (this.centerBalance === 1) {
                    myCarousel.css('left', this.getLeft());
                } else
                    this.centerBalance++;
            } else {
                $('#wooCarouselArrow-next').hide();
                $('.wooCarouselArrow-next').hide();
            }
        }
    }
}

function movement_activation(carousel, currentVideoIndex) {
    var toActiveItem = $('#' + carousel + ' > .carousel-item:nth-child(' + (currentVideoIndex) + ')');
    toActiveItem.addClass('active');
    var zeroStartCurrentVideoIndex = currentVideoIndex - 1;
    CarouselService.setMainShowByItem(
        CarouselGridService.getCurrentFocusedCarousel().itemsArr[zeroStartCurrentVideoIndex]);
}