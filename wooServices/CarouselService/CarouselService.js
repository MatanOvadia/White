
app.directive('carouselImageonload', function () {
        return {
            restrict: 'A',
            link: function ($scope, element) {
                element.bind('load', function () {
                    CarouselService.imgLoadCounter++;
                    if (CarouselService.imgLoadCounter > 5) {
                        AllowPressService.allow(switchersName.carouselPutItems);
                    }
                });
                element.bind('error', function () {
                    CarouselService.imgLoadCounter++;
                    if (CarouselService.imgLoadCounter > 5) {
                        AllowPressService.allow(switchersName.carouselPutItems);
                    }
                });
            }
        }
    }
);

var CarouselService = {

    arrSize: 0,

    imgLoadCounter: 0,

    setMainShowByItem: function (item) {
        console.log('Number(item.videoDuration');
        console.log(FormatterService.secondsToMinutes(Number(item.videoDuration)));
        console.log(Number(item.videoDuration));
        OnFocusedService.setMainShow(
            item.streamURL,
            item.title,
            $(item.description).text(),
            item.cover,
            FormatterService.secondsToMinutes(Number(item.videoDuration)),
            item.videoQuality);
    },

    getVideoItemDiv: function (i, carouselIndex) {
        return '<div' +
            ' myIndex="' + i + '"' +
            ' myAnyviewIndex="' + carouselIndex + '_' + i + '"' +
            ' class="video-item carousel-item"' +
            ' ng-click="playOnClick($event)">';
    }

};