const FRRR = 'FRRR';
const FRR = 'FRR';
const FR = 'FR';
const PLAY = 'PLAY';
const FF = 'FF';
const FFF = 'FFF';
const FFFF = 'FFFF';

function PlayerPlayback(player){

    return {
        intervalID: 0,

        playBackRateIndex: 3,

        PlayBackRateArr: [FRRR, FRR, FR, PLAY, FF, FFF, FFFF],

        currentTimeToSet: 0,

        reset: function () {
            var  _this = this;
            this.playBackRateIndex = 3;
            scope.infoBarSpeed = "";
            clearInterval(this.intervalID);

            this.intervalID = setInterval(function () {

                var currentTime = 0;
                if (player.isPlaying())
                    currentTime = player.getCurrentTime(); // TODO implement all players
                else
                    currentTime = _this.currentTimeToSet;

                var playerTotalTime = Math.floor(player.getDuration()); // TODO implement all players

                switch (_this.PlayBackRateArr[_this.playBackRateIndex]) {

                    case PLAY:
                        if (!scope.infoBarSpeed)
                            scope.infoBarSpeed = "";

                        if (scope.infoBarSpeed !== "") {
                            player.playContinue();
                        }
                        _this.currentTimeToSet = player.getCurrentTime();
                        console.log("_this.currentTimeToSet = " +_this.currentTimeToSet);
                        scope.infoBarSpeed = "";
                        return;

                    case FRRR:
                        if (scope.infoBarSpeed !== "-16") {
                            console.log('player.pause() -16');
                            player.pause();
                        }
                        if (currentTime - 16 > 0) {
                            scope.infoBarSpeed = "-16";
                            _this.currentTimeToSet = currentTime - 16;
                        } else {
                            console.log('player.pause() -16 2');
                            player.pause();
                            _this.currentTimeToSet = 0;
                            scope.infoBarSpeed = "";
                        }
                        break;

                    case FRR:
                        if (scope.infoBarSpeed !== "-8") {
                            console.log('player.pause() -8');
                            player.pause();
                        }
                        if (currentTime - 8 > 0) {
                            scope.infoBarSpeed = "-8";
                            _this.currentTimeToSet = currentTime - 8;
                        }
                        else {
                            player.pause();
                            this.currentTimeToSet = 0;
                            scope.infoBarSpeed = "";
                        }
                        break;

                    case FR:
                        if (scope.infoBarSpeed !== "-4") {
                            console.log('player.pause() -4');
                            player.pause();
                        }
                        if (currentTime - 4 > 0) {
                            scope.infoBarSpeed = "-4";
                            _this.currentTimeToSet = currentTime - 4;
                        } else {
                            player.pause();
                            _this.currentTimeToSet = 0;
                            scope.infoBarSpeed = "";
                        }
                        break;

                    case FF:
                        if (scope.infoBarSpeed !== "X4")
                            player.pause();
                        scope.infoBarSpeed = "X4";
                        _this.currentTimeToSet = currentTime + 4;
                        break;

                    case FFF:
                        if (scope.infoBarSpeed !== "X8")
                            player.pause();
                        scope.infoBarSpeed = "X8";
                        _this.currentTimeToSet = currentTime + 8;
                        break;

                    case FFFF:
                        if (scope.infoBarSpeed !== "X16")
                            player.pause();
                        scope.infoBarSpeed = "X16";
                        _this.currentTimeToSet = currentTime + 16;
                        break;
                }

                if (playerTotalTime < _this.currentTimeToSet) {
                    player.stop();
                    return;
                }

                safeDigest();

                if (scope.infoBarSpeed !== "") {
                    console.log("seeking to:" + _this.currentTimeToSet);
                    player.seekTo(_this.currentTimeToSet, true); // TODO implement all players
                }
                _this.updateProgressBar(_this.currentTimeToSet);
            }, 1000)
        },

        fastForward: function () {
            console.log('playback fastForward');
            if (this.playBackRateIndex < this.PlayBackRateArr.length - 1)
                this.playBackRateIndex++;
        },

        fastRewind: function () {
            console.log('fastRewind');
            if (this.playBackRateIndex > 0)
                this.playBackRateIndex--;
        },

        updateProgressBar: function (playerCurrentTime) {
            var playerTotalTime = Math.floor(player.getDuration());
            InfoBar2.progress(playerCurrentTime, playerTotalTime);
        }
    }
}