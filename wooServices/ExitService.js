const ExitService = {

    hide: function () {
        if (exitDialog.isShown){
            exitDialog.hide();
        }
    },

    exitApp: function () {
        if (tizenMode)
            tizen.application.getCurrentApplication().exit();
        if (LGMode) {
            webOS.platformBack();
            exitDialog.hide();
        } else
            window.history.back();
    },

    exit: function () {
        DialogService.defineDualDialogScope(
            'Are you sure you want to exit?',
            'Yes',
            'exitDialog_pressYes',
            'No',
            'exitDialog_pressNo');
        exitDialog.show();
    },

    configVisibilityChange: function () {
        if (tizenMode)
            document.addEventListener('visibilitychange', function () {
                if (document.hidden) {
                    if (tizenMode) {
                        VideoPlayer.pause();
                        tizen.application.getCurrentApplication().hide();
                    }
                } else {
                    switch (scope.currentTab) {
                        case liveTab:
                            // TODO matan pause live playing
                            console.log('// TODO matan pause live playing');
                            break;

                        case fourKTab:
                            break;

                        case radioLiveTab:
                            AudioPlayer.stop();
                            break;

                        case musicChannelsTab:
                            AudioPlayer.playContinue();
                            break;

                    }
                }
            });
    }
};
var exitDialog;

function initExitDialog() {

    exitDialog = {

        isShown: false,

        show: function () {
            exitDialog.isShown = true;
            scope.showDualDialog = true;
            scope.safeDigest();
            FocusService.unfocusCurrent();
            FocusService.focusByElementId('exitDialog_no');
        },

        hide: function () {
            if(!VideoPlayer.isActive){
                FocusService.unfocusCurrent();                FocusService.focusByElementId(FocusService.latestFocusedCarousel.id);
                FocusService.latestFocusedCarousel.focus();
            }
            exitDialog.isShown = false;
            scope.showDualDialog = false;
            scope.safeDigest();
        }
    };
}

function exitDialog_yes_func() {
    ExitService.exitApp();
}