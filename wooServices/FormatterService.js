const FormatterService = {

    fixSecondsTimeFormat: function (seconds) {
        if (scope.currentTab === liveTab ||
            scope.currentTab === arabTab ||
            scope.currentTab === mondialTab) {
            return "Live";
        }
        if (seconds !== undefined && seconds !== null && !isNaN(seconds)) {
            var date = new Date(null);
            date.setSeconds(seconds); // specify value for SECONDS here
            var formatted = date.toISOString();
            if (formatted[12] !== '0')
                return formatted.substr(12, 7);
            else {
                $('#infoBar_progressTime').css('font-size', '2.2vh');
                return formatted.substr(14, 5);
            }
        }
    },

    // making 2018-04-21T10:11:00 to 10:11
    fixCalendarTimeToHours: function (time) {
        if (time)
            return time.substring(time.length - 8, time.length - 3);
        else
            return "";
    },

    secondsToMinutes: function (s) {
        if (s && s > 0){
            console.log(Math.ceil(s / 60) + 'm');
            return Math.ceil(s / 60) + 'm';
        }

        else
            return 'null';
    }
};