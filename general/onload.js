function initAfterWindowIsLoaded() {

    VideoPlayer.initPlayers();

    HttpOutService.initConfig();

    HttpOutService.initCarousels();

    ExitService.configVisibilityChange();

    InternetConnectionHandler.initListeners();

    AllowPressService.allow(switchersName.onload);
}

function onWindowLoad() {
    AllowPressService.dontAllow(switchersName.onload);

    if (typeof tizen !== 'undefined') {
        utils.importJavascriptFile('$WEBAPIS/webapis/webapis.js', function () {
            log('SAMSUNG $WEBAPIS loaded');
        });
    }

    if (document.readyState === "complete")
        initAfterWindowIsLoaded();
    else
        setTimeout(onWindowLoad, 100)
}