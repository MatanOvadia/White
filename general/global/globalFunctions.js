var onloadFinished = false;

Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
};

function iniAppConfiguration() {

    WooJwPlayer.hide();

    body = $('body');

    if (tizenMode)
        body.addClass('tizen17');

    if (tizenMode && tizen2016)
        body.addClass('tizen');

    if (tizen2015)
        body.addClass('tizen2015');

    initExitDialog();

    onWindowLoad();

    MouseService.allowClicks();

    onloadFinished = true;
    AllowPressService.allow(switchersName.onload);
}

function safeDigest() {
    scope.safeDigest();
}

function log(content) {
    if (!allowLogs)
        return;
    var time = new Date();
    var timeFormat = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();

    if (typeof content === 'object') {
        console.warn("[" + timeFormat + "] object:");
        console.warn(content);
    } else console.warn("[" + timeFormat + "] " + content);
}

function logError(content) {
    if (!allowLogs)
        return;
    var time = new Date();
    var timeFormat = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();

    if (typeof content === 'object') {
        console.error("[" + timeFormat + "] object:");
        console.error(content);
    } else console.error("[" + timeFormat + "] " + content);
}

function fixYtThumb(itemWithThumb) {
    if (itemWithThumb)
        for (var i = 0; i < itemWithThumb.length; i++) {
            if (itemWithThumb[i].yt_thumb)
                if (itemWithThumb[i].yt_thumb.indexOf('http://') === -1)
                    itemWithThumb[i].yt_thumb = 'http:' + itemWithThumb[i].yt_thumb;
                else
                    return "";
        }
    else
        return "";
}

function playFrame3URL(direction, addToIndex, itemsArr, carouselId) {
    var dataIndex = $('#' + carouselId + ' li.frame3').attr('data-index');
    var numOfItems = itemsArr.length;
    dataIndex = parseInt(dataIndex);
    if (!addToIndex)
        addToIndex = 0;
    dataIndex += addToIndex;
    dataIndex = fixDataIndex(dataIndex, itemsArr.length);
    if (scope.currentTab === radioLiveTab)
        EpgService.setNowNextShows(radioLiveData[dataIndex].calender);
    AudioPlayer.play(itemsArr, dataIndex, 'aac', direction);
}

function fixDataIndex(dataIndex, numOfItems) {
    if (dataIndex < 0)
        return numOfItems - 1;
    else if (dataIndex === numOfItems)
        return 0;
    else return dataIndex;
}

const imgPrefix = 'http://kan.woo.media/uploads/thumbs/';


function fixImage(item, isYt) {
    if (isYt) {
        if (itShouldBeFixed(item.yt_thumb))
            return imgPrefix + item.yt_thumb;
        else if (item.yt_thumb.indexOf("i.ytimg.com") !== -1)
            return 'http:' + item.yt_thumb;
        else
            return item.yt_thumb;
    }
    else {
        if (itShouldBeFixed(item.image))
            return imgPrefix + item.image;
        else if (item.image.indexOf("i.ytimg.com") !== -1)
            return 'http:' + item.image;
        else
            return item.image;
    }
}

function itShouldBeFixed(imgUrl) {
    return imgUrl && imgUrl.indexOf("http://") === -1 &&
        imgUrl.indexOf("https://") === -1 &&
        imgUrl.indexOf("i.ytimg.com") === -1;
}