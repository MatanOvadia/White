
// paths
const paths = {
    allowAccessWorkaround: 'http://tools.woo.media/ImportantTools/allowAccessWorkaround.php',
    externalApiCorsProxyUrl: 'http://tools.woo.media/ImportantTools/externalApiCorsProxy.php'
};

const videoIdPrefix = "video-num-";

//  directions
const left = 'left';
const right = 'right';
const up = 'up';
const down = 'down';
const back = 'back';
const enter = 'enter';
const firstTime = 'firstTime';

// Pages
const homePage = 'homePage';
const tvPage = 'tvPage';
const radioPage = 'radioPage';
const searchPage = 'searchPage';
const header = 'header';
const content = 'content';
const tabContent = 'tabContent';
const page = 'page';
const radioLiveTab = 'radioLiveTab';

//  TV - tabs
const vodTab = 'vodTab';
const reshetShowsTab = 'reshetShowsTab';
const liveTab = 'liveTab';
const arabTab = 'arabTab';
const fourKTab = 'fourKTab';
const wooCarouselDiv = 'wooCarouselDiv';
const mondialTab = 'mondialTab';

const innerPage = 'innerPage';

const focused = 'focused';
const active = 'active';

var compile;
var scope;
var timeoutService;
var que;
var http;
var body;

const vw_to_vh = 1.777777778;
const vh_to_vw = 0.5625;
const px_to_vh = 0.09259259259;
const px_to_vw = 0.05208333333;
const vh_to_px = 10.8;
const vw_to_px = 19.2;

var active_color = "#00b4ff";
var carouselMovementSetup;

const playUrl = 'play-url';

const lastFocusedElement = "lastFocusedElement";

const innerPrefix = 'inner-';