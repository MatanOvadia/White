var usedKeys = ['MediaPause', 'MediaPlay', 'MediaPlayPause',
    'MediaFastForward', 'MediaRewind', 'MediaStop'];

if (tizenMode) {
    usedKeys.forEach(function (keyName) {
        tizen.tvinputdevice.registerKey(keyName);
    });
}

app.keyPressController = function () {

    scope.handleKeyDownAtFirstScreen = function (e) {

        if (allowPress) {

            IntervalService.lastKeyDownTimestamp = new Date();
            if (e.which !== 122 & e.which !== 123 && e.which !== 116) {// F11, F12, F5
                e.preventDefault();
                switch (e.which) {

                    case 8: // Backspace
                        // backspacePressed();
                        break;

                    case 402 : //Pause/play
                    case 10252: // Tizen pause/play
                        VideoPlayer.playPause();
                        break;

                    case 417: // MediaFastForward
                        if (scope.currentPage === tvPage)
                            if (tvPageManager.lastPlayerType === playerType.ytPlayer)
                                PlaybackService.fastForward();
                        break;

                    case 412: //MediaRewind
                        if (scope.currentPage === tvPage)
                            if (tvPageManager.lastPlayerType === playerType.ytPlayer)
                                PlaybackService.fastRewind();
                        break;

                    case 415: // Tizen play
                        console.log('Tizen play 415');
                        playPressed();
                        break;

                    case 19: // Tizen pause
                        console.log('Tizen pause 415');
                        pausePressed();
                        break;

                    case 37://LEFT KEY
                        leftPressed();
                        break;

                    case 39://RIGHT KEY
                        rightPressed();
                        break;

                    case 38://UP KEY
                        upPressed();
                        break;

                    case 40://DOWN KEY
                        downPressed();
                        break;

                    case 13://ENTER KEY
                        enterPressed();
                        break;

                    case 27  : //ESC
                    case 461 : // LG/Netgem back
                    case 10009: // Tizen back
                        backPressed();
                        break;
                }
            }
        }
    };

    $(document).on("keydown ", "html", function (e) {
        scope.handleKeyDownAtFirstScreen(e);
    });

};