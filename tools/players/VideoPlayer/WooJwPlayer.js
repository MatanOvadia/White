

var player;

var WooJwPlayer = {

    shouldPlay: false,
    duration: 0,

    play: function (url, type, isLive, onErrorCb) {

        log('jwplayer play url:' + url);

        AllowPressService.dontAllow(switchersName.jwPlay);
        var firstTime = true;

        scope.spinnerPreloaderShow = true;
        scope.safeDigest();
        $('#myjwplayer').css('display', 'block');
        console.log('playing url: ' + url + ' \n of type: ' + type);
        player = jwplayer('myjwplayer');

        player.setup({
            "playlist": [
                {
                    "file": url
                }
            ],
            'controls': 'false',
            'preload': 'auto'
        });

        player.on('setupError', function () {
            MouseService.allowClicks();
            logError('woo jwplayer setupError');
            AllowPressService.allow(switchersName.jwPlay);
            DialogService.playingError();
            WooJwPlayer.hide();
            MouseService.unbindClickEvents();
        });

        player.on('playAttemptFailed', function () {
            MouseService.allowClicks();
            logError('woo jwplayer playAttemptFailed');
            DialogService.defineDualDialogScope(
                'השידור אינו זמין כעת',
                'דף הבית',
                'exitDialog_pressYes',
                'נסה שנית',
                'exitDialog_pressNo');
            exitDialog.show();
            WooJwPlayer.hide();
        });


        player.on('ready', function () {
            log('jw player ready and trigger play');
            player.play(true);

        });

        player.on('beforeComplete', function () {
            console.log('before complete');
            WooJwPlayer.pause(true);
        });

        player.onTime(function (event) {
            if (event.position > 0.1) {
                if (firstTime) {
                    log('on time first time');
                    MouseService.allowClicks();
                    InfoBar2.initInfoBarState();
                    FocusService.unfocusCurrent();
                    FocusService.focusByElementId(infoBar2Id);
                    firstTime = false;
                    scope.spinnerPreloaderShow = false;
                    console.log('showInfoBar = true');
                    log('show info bar true');
                    scope.showInfoBar = true;
                    safeDigest();
                    if (isLive)
                        WooJwPlayer.duration = Infinity;
                    else
                        WooJwPlayer.duration = Math.floor(WooJwPlayer.getDuration());
                    AllowPressService.allow(switchersName.jwPlay);
                }
                InfoBar2.progress(event.position, WooJwPlayer.duration);
            }
        });

        player.on('error', function (e) {
            logError('jw player throw an error');
            logError(e);
            AllowPressService.allow(switchersName.jwPlay);
            scope.spinnerPreloaderShow = false;
            scope.safeDigest();
            if (onErrorCb) {
                log('jwPlayer onErrorCb');
                onErrorCb();
            } else {
                log('jwPlayer DialogService.defineDualDialogScope');
                DialogService.playingError();
                WooJwPlayer.hide();
            }
        });
    },

    playContinue: function () {
        console.warn('play continue');
        console.warn('showInfoBar = true');
        scope.showInfoBar = true;
        scope.safeDigest();
        InfoBar2.focusPause();
        player.setMute(false);
        player.setPlaybackRate(1);
        player.play(true);
    },

    pause: function (andShowInfoBar) {
        console.warn("pause jw player");
        InfoBar2.focusPlay();
        if (andShowInfoBar) {
            console.log('showInfoBar = true');
            log('show info bar true');
            scope.showInfoBar = true;
        }

        scope.spinnerPreloaderShow = false;
        scope.safeDigest();
        if (player)
            player.play(false);
    },

    stop: function () {
        InfoBar2.clear();
        scope.showInfoBar = false;
        WooJwPlayer.pause(false);
        WooJwPlayer.hide();
        scope.safeDigest();
    },

    hide: function () {
        log("hiding jw player");
        $('#myjwplayer').css('display', 'none');
    },

    isPlaying: function () {
        return player.getState() === 'playing';
    },

    playPause: function () {
        if (WooJwPlayer.isPlaying()) {
            InfoBar2.focusPlay();
            console.log('showInfoBar = true');
            log('show info bar true');
            scope.showInfoBar = true;
            scope.safeDigest();
            WooJwPlayer.pause(true);
        } else {
            console.log('showInfoBar = true');
            log('show info bar true');
            scope.showInfoBar = true;
            scope.safeDigest();
            InfoBar2.focusPause();
            WooJwPlayer.playContinue();
        }
    },

    getCurrentTime: function () {
        return player.getPosition();
    },

    getDuration: function () {
        return player.getDuration();
    },

    // Allowed 0.5 to 4
    upPlaybackRate: function () {
        var currentPlaybackRate = player.getPlaybackRate();
        switch (currentPlaybackRate) {
            case 1:
                scope.speed = 2;
                player.setPlaybackRate(2);
                break;
            case 0.5:
                scope.speed = 1;
                player.setPlaybackRate(1);
                break;
        }
        scope.safeDigest();
    },

    downPlaybackRate: function () {
        var currentPlaybackRate = player.getPlaybackRate();
        switch (currentPlaybackRate) {
            case 2:
                scope.speed = 1;
                player.setPlaybackRate(1);
                break;
            case 1:
                scope.speed = 0.5;
                player.setPlaybackRate(0.5);
                break;
        }
        scope.safeDigest();
    },

    // time in seconds
    seekTo: function (time) {
        player.setMute(true);
        player.seek(time);
        // WooJwPlayer.pause();

    }
};