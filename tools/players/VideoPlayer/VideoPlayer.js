const playerTypes = {
    jw: {
        name: 'jw',
        playback: new PlayerPlayback(WooJwPlayer)
    },
    yt: {
        name: 'yt'
    },
    av: {
        name: 'av'
    }
};

const VideoPlayer = {

    lastPlayerType: null,

    isActive: false,

    initPlayers: function () {

        if (!tizenMode) {
            $('#av-player').css('display', 'none');
        }

        WooYoutubePlayer.initYoutubePlayer();
    },

    playPause: function () {
        switch (this.lastPlayerType) {
            case playerTypes.jw:
                WooJwPlayer.playPause();
                break;
        }
        VideoPlayer.lastPlayerType.playback.reset();
    },

    exit: function () {
        log('VideoPlayer exit');
        VideoPlayer.isActive = false;
        if (tizenMode) {
            AvVideoPlayer.stop();
            scope.showInfoBar = false;
            scope.safeDigest();
        } else {
            if (WooYoutubePlayer.activation.isActive())
                WooYoutubePlayer.stop();
            else {
                WooJwPlayer.setFullscreen(false);
                WooJwPlayer.stop();
            }
        }
    }
    ,

    freeze: function () {
        log('VideoPlayer freeze');
        if (VideoPlayer.isActive) {
            MouseService.unbindClickEvents();
            VideoPlayer.pause();
        }
        log('exit VideoPlayer freeze');
    }
    ,

    resume: function () {
        log('VideoPlayer resume');
        if (WooYoutubePlayer.activation.isActive())
            WooYoutubePlayer.playContinue();
        else if (tizenMode)
            AvVideoPlayer.playContinue();
        else
            WooJwPlayer.playContinue();
        FocusService.unfocusCurrent();
        FocusService.focusByElementId(infoBar2Id);
        log('exit VideoPlayer resume');
    }
    ,

    pause: function () {
        log('VideoPlayer pause');
        if (VideoPlayer.isActive) {
            if (tizenMode)
                AvVideoPlayer.pause();
            else if (WooYoutubePlayer.activation.isActive())
                WooYoutubePlayer.pause();
            else
                WooJwPlayer.pause();
        }
    },

    playAndShowInfoBar: function (urlToPlay) {
        scope.elementBeforePlay = FocusService.getCurrentFocusedElementId();
        safeDigest();
        VideoPlayer.play(urlToPlay);
        FocusService.unfocusCurrent();
        FocusService.focusByElementId(infoBar2Id);
    }
    ,

    play: function (urlToPlay, isLive) {

        log('VideoPlayer play, url = ' + urlToPlay);

        if (isLive) {
            InfoBar2.clear(isLive);
        }

        scope.spinnerPreloaderShow = true;
        scope.safeDigest();

        if (urlToPlay.indexOf('youtube') !== -1) {
            WooYoutubePlayer.play(urlToPlay);
            this.lastPlayerType = playerTypes.yt;
        }

        else if (tizenMode) {
            this.lastPlayerType = playerTypes.av;
            AvVideoPlayer.play(urlToPlay, isLive);
        } else {
            this.lastPlayerType = playerTypes.jw;
            WooJwPlayer.play(urlToPlay, null, isLive);
        }

        this.lastPlayerType.playback.reset();
        MouseService.bindLiveEvents();
        VideoPlayer.isActive = true;
    },

    stop: function () {
        MouseService.unbindClickEvents();
        switch (this.lastPlayerType) {
            case playerTypes.jw:
                WooJwPlayer.stop();
                break;
        }
    },

    fastForward: function () {
        // this.lastPlayerType.playback.fastForward();
    },

    fastRewind: function () {
        // switch (this.lastPlayerType) {
        //     case playerTypes.yt:
        //         PlaybackService.fastRewind();
        //         break;
        // }
    }
};