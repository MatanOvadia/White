const AvVideoPlayer = {

    player: null,

    // NOTE matan - don't remove even seems not in used - used for player
    listener: {
        oncurrentplaytime: function (currentTime) {
            InfoBar2.progress(currentTime, Infinity);
        },
        onstreamcompleted: function () {
            this.stop();
        }.bind(this),
        onerror: function (eventType) {
            console.warn("event type error : " + eventType);
        }
    },

    init: function () {
        AvVideoPlayer.player = document.getElementById('av-player');
        AvVideoPlayer.player.style.display = 'none';
    },

    play: function (urlToPlay, type, isLive) {

        console.log('AvVideoPlayer play');

        $('#av-player').css('visibility', 'visible');

        console.log('AvVideoPlayer play url:' + urlToPlay);

        webapis.avplay.setListener(AvVideoPlayer.listener);

        try {
            webapis.avplay.open(urlToPlay);
            webapis.avplay.setDisplayRect(0, 0, 1920, 1080);
        } catch (e) {
            console.warn(e);
        }

        if(isLive){
            if (scope.currentTab === mondialTab)
                webapis.avplay.setStreamingProperty("SET_MODE_4K", "true");
        }

        if (webapis.avplay.getState() === 'IDLE') {
            webapis.avplay.prepare();
            InfoBar2.initInfoBarState();
            webapis.avplay.play();
            console.log('showInfoBar = true');log('show info bar true');scope.showInfoBar = true;
            safeDigest();
        } else if (webapis.avplay.getState() === 'PAUSED'){
            InfoBar2.initInfoBarState();
            webapis.avplay.play();
            console.log('showInfoBar = true');log('show info bar true');scope.showInfoBar = true;
            safeDigest();
        }

        scope.spinnerPreloaderShow = false;
        scope.safeDigest();

        console.log('exit AvVideoPlayer play');
    },

    playContinue: function () {
        var playerState = webapis.avplay.getState();
        if (playerState === 'PAUSED' || playerState === 'READY') {
            InfoBar2.initInfoBarState();
            webapis.avplay.play();
            console.log('showInfoBar = true');log('show info bar true');scope.showInfoBar = true;
            scope.safeDigest();
            InfoBar2.focusPause();
        }
    },

    pause: function () {
        var playerState = webapis.avplay.getState();
        if (playerState === 'PLAYING' || playerState === 'READY') {
            webapis.avplay.pause();
            InfoBar2.focusPlay();
            console.log('showInfoBar = true');log('show info bar true');scope.showInfoBar = true;
            scope.safeDigest();
        }
    },

    playPause: function () {
        var playerState = webapis.avplay.getState();
        if (playerState === 'PAUSED') {
            InfoBar2.initInfoBarState();
            webapis.avplay.play();
            console.log('showInfoBar = true'); log('show info bar true');scope.showInfoBar = true;
            scope.safeDigest();
            InfoBar2.focusPause();
        } else if (playerState === 'PLAYING' || playerState === 'READY') {
            webapis.avplay.pause();
            InfoBar2.focusPlay();
            console.log('showInfoBar = true');log('show info bar true');scope.showInfoBar = true;
            scope.safeDigest();
        }

    },

    stop: function () {
        webapis.avplay.stop();
        InfoBar2.clear();
        scope.showInfoBar = false;
        $('#av-player').css('visibility', 'hidden');
        scope.spinnerPreloaderShow = false;
        scope.safeDigest();
    },

    ff: function () {
        console.log('AvVideoPlayer ff');
        webapis.avplay.jumpForward('3000');
    },
    rew: function () {
        console.log('AvVideoPlayer rew');
        webapis.avplay.jumpBackward('3000');
    },

    setUhd: function (isEnabled) {
        isUhd = isEnabled;
    },

    set4K: function () {
        webapis.avplay.setStreamingProperty("SET_MODE_4K", "true");
    },
    /**
     * Function to set specific bitrates used to play the stream.
     * In case of Smooth Streaming STARTBITRATE and SKIPBITRATE values 'LOWEST', 'HIGHEST', 'AVERAGE' can be set.
     * For other streaming engines there must be numeric values.
     *
     * @param {Number} from  - Lower value of bitrates range.
     * @param {Number} to    - Higher value of the bitrates range.
     * @param {Number} start - Bitrate which should be used for initial chunks.
     * @param {Number} skip  - Bitrate that will not be used.
     */
    setBitrate: function (from, to, start, skip) {
        var bitrates = '|BITRATES=' + from + '~' + to;

        if (start !== '' && start !== undefined) {
            bitrates += '|STARTBITRATE=' + start;
        }
        if (to !== '' && to !== undefined) {
            bitrates += '|SKIPBITRATE=' + skip;
        }

        webapis.avplay.setStreamingProperty("ADAPTIVE_INFO", bitrates);
    },

    toggleFullscreen: function () {
        if (isFullscreen === false) {
            webapis.avplay.setDisplayRect(0, 0, 1920, 1080);
            player.classList.add('fullscreenMode');
            isFullscreen = true;
        } else {
            log('Fullscreen off');
            try {
                webapis.avplay.setDisplayRect(
                    playerCoords.x,
                    playerCoords.y,
                    playerCoords.width,
                    playerCoords.height
                );
            } catch (e) {
                console.warn(e);
            }
            player.classList.remove('fullscreenMode');
            isFullscreen = false;
        }
    }
};