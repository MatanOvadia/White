var youtubePlayer;

var WooYoutubePlayer = {

    private: {

        myTimer: null,

        onPlayerStateChange: function (event) {
            console.log('youtube player state changed');
            console.log(event);
            console.log('youtubePlayer.getPlayerState()');
            console.log(youtubePlayer.getPlayerState());
            if (event.data === YT.PlayerState.PLAYING) {
                InfoBar2.initInfoBarState();
                var playerTotalTime = Math.floor(youtubePlayer.getDuration());
                WooYoutubePlayer.private.myTimer =
                    setInterval(function () {
                        var playerCurrentTime = Math.floor(youtubePlayer.getCurrentTime());
                        InfoBar2.progress(playerCurrentTime, playerTotalTime);
                    }, 1000);
            } else if (event.data === YT.PlayerState.ENDED) {
                InfoBar2.clear();
                WooYoutubePlayer.activation.set(false);
                FocusService.unfocusCurrent();
                switch (scope.currentTab) {
                    case reshetShowsTab:
                        FocusService.focusByElementId(reshetShowsTab);
                        break;

                    case vodTab:
                        FocusService.focusByElementId(vodTab);
                        break;

                    case fourKTab:
                        FocusService.focusByElementId(fourKTab);
                        break;
                }
                clearTimeout(WooYoutubePlayer.private.myTimer);
            } else
                clearTimeout(WooYoutubePlayer.private.myTimer);
            console.log('youtubePlayer.getPlayerState()');
            console.log(youtubePlayer.getPlayerState());
        }
    },

    initYoutubePlayer: function () {
        youtubePlayer = new YT.Player('myYoutubePlayer', {
            height: '100%',
            width: '100%',
            playerVars: {
                controls: 0,
                rel: 0,
                showinfo: 0
            },
            events: {
                'onStateChange': WooYoutubePlayer.private.onPlayerStateChange
            }
        });
        WooYoutubePlayer.activation.set(false);
    },

    play: function (url) {
        InfoBar2.clear();
        var re = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*?[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
        var idOfYoutubeVideo = url.replace(re, '$1');
        WooYoutubePlayer.playId(idOfYoutubeVideo);
    },

    playId: function (idOfYoutubeVideo) {
        if (!WooYoutubePlayer.isPlaying()) {
            WooYoutubePlayer.activation.set(true);
            youtubePlayer.loadVideoById(idOfYoutubeVideo);
            MouseService.bindYtClickEvents();
        }
    },

    playContinue: function () {
        console.log('showInfoBar = true');
        log('show info bar true');
        scope.showInfoBar = true;
        scope.safeDigest();
        InfoBar2.focusPause();
        youtubePlayer.playVideo()
    },

    pause: function () {
        log('pause youtube player');
        InfoBar2.focusPlay();
        console.log('showInfoBar = true');
        log('show info bar true');
        scope.showInfoBar = true;
        scope.safeDigest();
        youtubePlayer.pauseVideo();
        log('after pause youtube player');
    },

    stop: function () {
        MouseService.unbindClickEvents();
        clearInterval(PlaybackService.intervalID);
        InfoBar2.clear();
        var lastElementId = scope.lastFocusedElement;
        log('lastElementId = ');
        log(lastElementId);
        FocusService.unfocusCurrent();
        FocusService.focusByElementId(lastElementId);
        youtubePlayer.pauseVideo();
        WooYoutubePlayer.activation.set(false);
    },

    playPause: function () {
        if (WooYoutubePlayer.isPlaying()) {
            InfoBar2.focusPlay();
            console.log('showInfoBar = true');
            log('show info bar true');
            scope.showInfoBar = true;
            scope.safeDigest();
            youtubePlayer.pauseVideo();
        } else {
            console.log('showInfoBar = true');
            log('show info bar true');
            scope.showInfoBar = true;
            scope.safeDigest();
            InfoBar2.focusPause();
            youtubePlayer.playVideo();
        }
    },

    isPlaying: function () {
        if (youtubePlayer)
            return youtubePlayer.getPlayerState() === 1;
        else
            return false;
    },

    activation: {

        set: function (activeState) {
            if (activeState)
                $('#myYoutubePlayer').css('display', 'block');
            else
                $('#myYoutubePlayer').css('display', 'none');
        },

        isActive: function () {
            return $('#myYoutubePlayer').css('display') === 'block';
        }
    },

    isFullscreen: function () {
        return !!WooYoutubePlayer.isPlaying();
    }
};