var current_app_id = 'UA-90473457-7'; // holy places

if (tizenMode) {
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        },
            i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', current_app_id, 'auto');
    ga(function (tracker) {
        $.get("https://www.google-analytics.com/collect?" +
            "v=1" +
            "&t=pageview" +
            "&cid=" + tracker.get('clientId') +
            "&tid=" + current_app_id +
            "&dl=http%3A%2F%holyplaces.woo.media%2Fsmarttv%2F" +
            "&_gid=" + tracker.get('&_gid') +
            "&z=" + tracker.get('&z'));
    });
} else {
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        },
            i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', current_app_id, 'auto'); // utab 106845716-1
    ga('send', 'pageview');
}