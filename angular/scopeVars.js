function initScopeVars() {

    scope.ids = {
        homePageIcon: "homePage_homeIcon"
    };

    scope.orderStrings = {
        tabContent: tabContent
    };

    scope.carousels = [];
}

function initScopeFunctions() {

    scope.safeDigest= function () {
        timeoutService(function () {
            scope.$digest();
        })
    };

}