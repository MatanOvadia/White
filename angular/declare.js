app.directive('back', function () {
    return {
        restrict: 'A'
    }
});

app.directive('right', function () {
    return {
        restrict: 'A'
    }
});
app.directive('left', function () {
    return {
        restrict: 'A'
    }
});
app.directive('up', function () {
    return {
        restrict: 'A'
    }
});
app.directive('down', function () {
    return {
        restrict: 'A'
    }
});
app.directive('innerDown', function () {
    return {
        restrict: 'A'
    }
});
app.directive('innerBack', function () {
    return {
        restrict: 'A'
    }
});
app.directive('innerEnter', function () {
    return {
        restrict: 'A'
    }
});


app.directive('enter', function () {
    return {
        restrict: 'A'
    }
});

app.directive('nextPage', function () {
    return {
        restrict: 'A'
    }
});

app.directive('nextTab', function () {
    return {
        restrict: 'A'
    }
});

app.directive('playUrl', function () {
    return {
        restrict: 'A'
    }
});

app.directive('seriesId', function () {
    return {
        restrict: 'A'
    }
});

app.directive('gridId', function () {
    return {
        restrict: 'A'
    }
});

app.directive('gridItemPrefix', function () {
    return {
        restrict: 'A'
    }
});

app.directive('spinnerPreloader', function () {
    return {
        templateUrl: 'directives/common/spinnerPreloader.html',
        restrict: 'E'
    }
});

app.directive('onFocused', function () {
    return {
        restrict: 'A'
    }
});

app.directive('onUnfocused', function () {
    return {
        restrict: 'A'
    }
});

app.directive('myNext', function () {
    return {
        restrict: 'A'
    }
});

app.directive('myEnterNext', function () {
    return {
        restrict: 'A'
    }
});

app.directive('myInit', function () {
    return {
        restrict: 'A'
    }
});

app.directive('myPlay', function () {
    return {
        restrict: 'A'
    }
});

app.directive('myPause', function () {
    return {
        restrict: 'A'
    }
});

app.directive('dontTouch', function () {
    return {
        restrict: 'A'
    }
});

app.directive('splashScreen', ['$timeout', function($timeout){
    return {
        restrict : 'E',
        link : function(myScope, elem, attr){
            $timeout(function(){
                elem.remove();
                scope.showGifSplashScreen = false;
                safeDigest();
            }, 4000);
        }
    }
}]);