const InternetConnectionHandler = {

    isConnected: true,

    disconnect: function () {
        if (InternetConnectionHandler.isConnected) {
            InternetConnectionHandler.isConnected = false;
            MouseService.unbindClickEvents();
            VideoPlayer.freeze();
            DialogService.defineSingleDialogScope();
            FocusService.unfocusCurrent();
            FocusService.focusByElementId('singleOptionDialogBtn');
            scope.showSingleOptionDialog = true;
            safeDigest();
        }
    },

    resume: function () {
        if (!InternetConnectionHandler.isConnected) {
            MouseService.allowClicks();
            InternetConnectionHandler.isConnected = true;
            if (VideoPlayer.isActive) {
                MouseService.bindLiveEvents();
                VideoPlayer.resume();
            }
            else {
                FocusService.unfocusCurrent();
                FocusService.focusByElementId(scope.ids.homePageIcon);
            }
            DialogService.hideNotConnected();
        }
    },

    checkConnectionFrequency: 1000 * 3,

    initListeners: function () {
        if (tizenMode) {
            webapis.network.addNetworkStateChangeListener(function (value) {
                if (value === webapis.network.NetworkState.GATEWAY_DISCONNECTED)
                    InternetConnectionHandler.disconnect();
                else if (value === webapis.network.NetworkState.GATEWAY_CONNECTED)
                    InternetConnectionHandler.resume();
            });
        } else {
            setInterval(function () {
                if (LGMode) {
                    webOS.service.request("luna://com.webos.service.connectionmanager", {
                        method: "getStatus",
                        onSuccess: function (inResponse) {
                            if (inResponse.isInternetConnectionAvailable)
                                InternetConnectionHandler.resume();
                            else
                                InternetConnectionHandler.disconnect();
                        },
                        onFailure: function (inError) {
                            console.log("Failed to get network state");
                            console.log("[" + inError.errorCode + "]: " + inError.errorText);
                        }
                    })
                } else {
                   // HttpOutService.checkConnectionByHttpCall();
                }
            }, 10000);
        }
    }
};