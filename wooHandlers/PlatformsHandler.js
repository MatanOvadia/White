
const tizenMode = (navigator.userAgent.indexOf("Tizen") !== -1);

const tizen2017 = tizenMode &&
    navigator.userAgent.indexOf("Version/3.0") !== -1;

const tizen2016 = tizenMode &&
    navigator.userAgent.indexOf("Version/2.4") !== -1;

const tizen2015 = tizenMode &&
    navigator.userAgent.indexOf("Version/2.3") !== -1;

const LGMode = (navigator.userAgent.indexOf("Web0S") !== -1);

const screen1280 = (screen.width === 1280);