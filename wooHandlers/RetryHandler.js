const RetryHandler = {

    reloadTab: function () {
        var keepCurrentTab = scope.currentTab;
        scope.currentTab = "";
        scope.safeDigest();
        timeoutService(function () {
            scope.currentTab = keepCurrentTab;
            scope.safeDigest();
        }, 0, false);
    }
};